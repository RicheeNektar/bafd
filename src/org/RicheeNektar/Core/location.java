package org.RicheeNektar.Core;

import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.json.simple.JSONObject;

public class location implements Serializable {
	private static final long serialVersionUID = 1L;
	
	int x,y,z;
	float pitch,yaw;
	String w;
	public location(Location loc) {
		x = loc.getBlockX();
		y = loc.getBlockY();
		z = loc.getBlockZ();
		pitch = loc.getPitch();
		yaw = loc.getYaw();
		w = loc.getWorld().getName();
	}
	
	public Location getLocation() {
		return new Location(Bukkit.getWorld(w), x, y, z, pitch, yaw);
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		
		obj.put("world", w);
		obj.put("x", x);
		obj.put("y", y);
		obj.put("z", z);
		obj.put("pitch", pitch);
		obj.put("yaw", yaw);
		
		return obj;
	}
	
	public location(JSONObject o) {
		try {
			this.x = Integer.parseInt(o.get("x").toString());
			this.y = Integer.parseInt(o.get("y").toString());
			this.z = Integer.parseInt(o.get("z").toString());
			this.yaw = Float.parseFloat(o.get("yaw").toString());
			this.pitch = Float.parseFloat(o.get("pitch").toString());
			this.w = o.get("world").toString();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}
