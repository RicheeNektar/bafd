package org.RicheeNektar.Core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Base64;
import java.util.HashMap;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Core extends JavaPlugin implements Listener {
	
	private String fls = System.getProperty("file.separator");
	private File save = new File("plugins" + fls + this.getDescription().getName() + fls + "homes.json");
	
	private String prefix = "�4[�7WarpSys�4] �8- �f";
	
	private HashMap<String, location> warps = new HashMap<String, location>();
	
	public void update() {
		try {
			File f = Updater.download("plugins" + fls + getDescription().getName() + fls + "test.test", false);
			JarFile jf = new JarFile(f);
			ZipEntry entry = jf.getEntry("plugin.yml");
			BufferedReader reader = new BufferedReader(new InputStreamReader(jf.getInputStream(entry)));
			String s;
			
			int newV = -1;
			
			while((s=reader.readLine())!=null) {
				String[] reg = s.trim().split(":");
				if(reg[0].equalsIgnoreCase("version")) {
					newV = Integer.parseInt(reg[1].replaceAll("[.]", "").trim());
					break;
				}
			}
			reader.close();
			jf.close();
			f.delete();
			
			int thisv = Integer.parseInt(getDescription().getVersion().replaceAll("[.]", "").trim());
			
			if(newV>thisv) {
				Updater.download("", true);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onEnable() {
		try {
			if(!save.exists()) {
				save.createNewFile();
			} else {
				try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(save)))) {
					
					StringBuilder b = new StringBuilder();
					String s = "";
					while((s=reader.readLine())!=null) {
						for(byte by : Base64.getDecoder().decode(s)) {
							b.append((char)by);
						}
					}
					JSONObject root = (JSONObject) new JSONParser().parse(b.toString());
					
					for(Object o : root.keySet()) {
						warps.put(o.toString(), new location((JSONObject) root.get(o)));
					}
					
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		update();
	}
	
	@SuppressWarnings("unchecked")
	public void onDisable() {
		JSONObject root = new JSONObject();
		
		for(String key : warps.keySet()) {
			System.out.println(key + " - " + warps.get(key).toJSON().toJSONString());
			location loc = warps.get(key);
			root.put(key, loc.getLocation());
		}
		
		try {
			if(!save.exists()) {
				save.createNewFile();
			}
			
			try(BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(save)))) {
				writer.write(Base64.getEncoder().encodeToString(root.toJSONString().getBytes()));
			} catch(Exception ex) {
				ex.printStackTrace();
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		if(e.getInventory().getType()==InventoryType.ANVIL) {
			if(!(e.getInventory() instanceof AnvilInventory)) return;
			
			AnvilInventory inv = (AnvilInventory) e.getInventory();
			inv.setRepairCost(0);
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			
			if(args.length<1) {
				
				if(label.equalsIgnoreCase("update")) {
					p.sendMessage(prefix + "I will update if there is any update available ;)");
					update();
					return true;
				} else {
					p.sendMessage(prefix + "Du musst einen Warp Namen angeben!");
					return true;
				}
				
			}
			String name = args[0];
			boolean exist = warps.get(name)!=null;
			
			if(label.equalsIgnoreCase("setwarp")) {
				if(exist) {
					p.sendMessage(prefix + "Dieser Warp existiert bereits.");
					return true;
				}
				
				location loc = new location(p.getLocation());
				warps.put(args[0], loc);
				return true;
				
			} 
			if(label.equalsIgnoreCase("delwarp")||label.equalsIgnoreCase("remwarp")) {
				if(!exist) {
					p.sendMessage(prefix + "Dieser Warp existiert nicht.");
					return true;
				}
				
				warps.remove(args[0]);
				return true;
				
			} 
			if(label.equalsIgnoreCase("warp")) {
				if(!exist) {
					p.sendMessage(prefix + "Dieser Warp existiert nicht.");
					return true;
				}
				
				location loc = warps.get(args[0]);
				p.teleport(loc.getLocation());
				return true;
				
			}
		}
		return false;
	}
}
