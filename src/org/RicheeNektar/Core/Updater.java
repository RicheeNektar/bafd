package org.RicheeNektar.Core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.bukkit.Bukkit;

public class Updater {
	private static String fls = System.getProperty("file.separator");
	
	public static File download(String dest, boolean update) throws Exception {
		
		if(update) dest = "plugins" + fls + "Update";
		
		String uri = "https://www.dropbox.com/s/cbsz033cpb0zfa5/warp.jar?dl=1";
		URL url = new URL(uri);
		
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
		HttpURLConnection.setFollowRedirects(true);
		
		InputStream in = conn.getInputStream();
		
		File a = new File(dest);
		if(!a.exists()) {
			a.mkdirs();
		}
		dest += fls + "warp.jar";
		File b = new File(dest);
		
		OutputStream out = new FileOutputStream(b);
		int len = 0;
		byte[] buffer = new byte[4096];
		while((len=in.read(buffer))>0) {
			out.write(buffer, 0, len);
		}
		out.close();
		
		if(update) {
			Bukkit.broadcastMessage("New Update downloaded now Reloading!");
			Bukkit.reload();
		}
		return b;
	}
}
